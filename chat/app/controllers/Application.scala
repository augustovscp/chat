package controllers

import play.api.libs.EventSource
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.{Enumerator, Concurrent, Enumeratee}
import play.api.libs.json.{Json, JsValue}
import play.api.mvc._

object Application extends Controller {
  
  val (chatOut, chatChannel) = Concurrent.broadcast[JsValue]

  def index = Action { implicit req =>
    Ok(views.html.index(routes.Application.chatFeed, routes.Application.postMessage))
  }

  def connDeathWatch(addr: String): Enumeratee[JsValue, JsValue] =
    Enumeratee.onIterateeDone{ () => println(addr + " - desconectado") }

  def welcome = Enumerator.apply[JsValue](Json.obj(
    "user" -> "Pixeon",
    "message" -> "Bem Vindo! Digite uma mensagem para iniciar a conversa!"
  ))

  def chatFeed = Action { req =>
    println(req.remoteAddress + " - conectado")
    Ok.chunked(welcome >>> chatOut
      &> connDeathWatch(req.remoteAddress)
      &> EventSource()
    ).as("text/event-stream")
  }

  def postMessage = Action(parse.json) { req => chatChannel.push(req.body); Ok }

}